### LOGPROBE

Logprobe is a small daemon that searches for match lines in
named log files and once everything is considered done, it will
send a trigger condition string to the Pantavisor control socket.

Common usage for this is to gate/orchestrate the startup order of
containers between logical groups of the Pantavisor system state.

## logprobe daemon usage

logprobe can be run directly and it takes two arguments:
1. config file path
2. condition string to send

BY DEFAULT logprobe expects a config file at /etc/logprobe.config
and will not run without a condition

For example:

```
/path/to/logprobe /etc/probe.config trigger_group_1
```

# Config format

Config file takes a format that follows

```
$CONTAINER_NAME1;$PATH_TO_LOGFILE1;$MATCH_STRING1
$CONTAINER_NAME2;$PATH_TO_LOGFILE2;$MATCH_STRING2
...
$CONTAINER_NAMEn;$PATH_TO_LOGFILEn;$MATCH_STRINGn
```

It can take several of the above line and they will all be waited on
before signalling completion and thus sending the condition.
