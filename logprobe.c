/* Copyright 2022 Pantacor Ltd  */
/*   License: MIT 		*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <limits.h>
#include <sys/stat.h>
#include <sys/sysinfo.h>

#define cond_cmd "curl -X PUT --data \"true\" --unix-socket /pantavisor/pv-ctrl \
			 \"http://localhost/conditions/%s\""

struct probe {
	char *cname;
	char *logp;
	char *match;
	int fd;
	bool done;
	struct probe *next;
};

enum {
	CNAME = 0,
	LOGP,
	MATCH
};

struct probe *phead = NULL;

void usage()
{
	printf("Usage: probe [CONFIG_PATH] [CONDITION=first_group_ready]\n");
	exit(1);
}

int readline_and_match(int fd, char *match)
{
	int i = 0, found = 0;
	char line[2048];
	char c;

	int lines = 0;
	while (read(fd, line+i, 1)) {
		if (line[i] == '\n') {
			found = (strstr(line, match) != NULL);
			memset(line, 0, sizeof(line));
			i = 0;
			if (found)
				goto out;
		} else {
			i++;
		}
	}

out:
	return found;
}

void add_one(char *data)
{
	int idx = 0;
	char *token;
	struct probe *cur = phead;

	token = strtok(data, ";");

	if (!phead) {
		phead = calloc(1, sizeof(struct probe));
		cur = phead;
	} else {
		while (cur != NULL && cur->next != NULL)
			cur = cur->next;
		cur->next = calloc(1, sizeof(struct probe));
		cur = cur->next;
	}

	while (token != NULL) {
		switch (idx) {
		case CNAME:
			cur->cname = strdup(token);
			break;
		case LOGP:
			cur->logp = strdup(token);
			break;
		case MATCH:
			cur->match = strdup(token);
			break;
		default:
			break;
		}
		idx++;
		token = strtok(NULL, ";");
	}
	cur->done = false;
}

int open_all(void)
{
	int ret = 0;
	struct probe *p;

	p = phead;
	while (p) {
		if (p->fd <= 0) {
			p->fd = open(p->logp, O_RDONLY);
		} else {
			p = p->next;
			continue;
		}
		if (p->fd < 0) {
			printf("Unable to open '%s:%s', waiting\n", p->cname, p->logp);
			ret = 1;
			p = p->next;
		} else {
			printf("Opened '%s:%s'\n", p->cname, p->logp);
		}
	}

	return ret;
}

void run_one_probe(struct probe *p)
{
	struct sysinfo si;

	if(!readline_and_match(p->fd, p->match))
		return;

	p->done = true;
	sysinfo(&si);
	printf("Container '%s' finished bootup at '%ld' seconds since boot.\n",
		p->cname, si.uptime);
	fflush(stdout);
}

int main(int argc, char *argv[])
{
	FILE *c;
	int count = 0;
	struct stat sb;
	char cmd[PATH_MAX];
	char *data, *token;
	struct probe *p;
	struct sysinfo si;

	if (argc < 2)
		usage();

	c = fopen(argv[1], "r");

	if(stat(argv[1], &sb)) {
		printf("Unable to read config file.\n");
		exit(1);
	}

	data = malloc(sb.st_size);

	while (fscanf(c, "%[^\n] ", data) != EOF)
		add_one(data);

	fclose(c);

	while (open_all())
		sleep(1);

	p = phead;
	while (p) {
		count++;
		p = p->next;
	}

	printf("Waiting for %d containers to finish.\n", count);
	while (true) {
		p = phead;
		while (p != NULL) {
			run_one_probe(p);
			if (p->done)
				count--;
			p = p->next;
		}
		if (count <= 0)
			break;
		sleep(1);
	}

	sysinfo(&si);
	printf("All containers have finished bootup at '%ld' seconds since boot.\n",
		 si.uptime);

	sprintf(cmd, cond_cmd, argc == 3 ? argv[2] : "first_group_ready");
	system(cmd);
	exit(0);
}
